<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="row collapse">
        <div class="column">
            <input type="text" value="" name="s" id="s" tabindex="1" placeholder="search..." />
        </div>
        <div class="shrink column">
            <button href="#" class="button postfix" tabindex="2" type="submit">Go</button>
        </div>
    </div>
</form>
