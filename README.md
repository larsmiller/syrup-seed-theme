# Syrup Seed Theme
Author: [Syrup](https://syrupmarketing.com/)
## Getting Started

Install Node on local machine (tested with 6.9.0)  

Then, `cd /themedirectory/` and `npm install`

### Development  Compiling
`gulp watch`, `gulp build`, or simply `gulp`

### Production  Compiling
Before pushing code to dev or live server, run `gulp build --production`. This will compile and minify. Very important as non-local IP addresses enqueue the minified files.

### Advanced Custom Fields PRO
- required plugin
- once installed/activated, import the file syrup-seed-acf-starter.json as a starter

### Advanced Custom Fields PRO
- edit a page and add the Sample Component to see components in action
