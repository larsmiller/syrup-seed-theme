<?php
/**
 * @package WordPress
 * @subpackage syrup
 */

require_once locate_template('/lib/acf.php');
require_once locate_template('/lib/cleanup.php');
require_once locate_template('/lib/cpt.php');
require_once locate_template('/lib/init.php');
require_once locate_template('/lib/scripts.php');
require_once locate_template('/lib/social.php');
require_once locate_template('/lib/titles.php');
// require_once locate_template('/lib/vimeo.php');
