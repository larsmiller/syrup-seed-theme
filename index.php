<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
get_header();
rewind_posts();
?>
<div id="index-top">
    <div class="row align-center">
        <div class="columns small-12 medium-8 large-8">
            <?php
            $title = syrup_title();
            if ($title) {
                echo '<h2 class="text-center wow slide-in-up" id="index-title">'.$title.'</h2>';
            }
            ?>
            <div class="archive-list component-padding">
                <div class="row">
                    <div class="columns small-12 wow slide-in-up">
                        <div class="archive" id="archive">
                            <?php
                            if (have_posts()) {
                                while ( have_posts() ) {
                                    the_post();
                                    echo '<h3>'.get_the_title().'</h3>';
                                }
                            } else {
                                echo '<h3>No posts meet your search criteria.</h3>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php get_template_part('partials/pagination'); ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>
