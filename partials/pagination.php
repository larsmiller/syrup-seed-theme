<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
/* Display navigation to next/previous pages when applicable */
if (  $wp_query->max_num_pages > 1 ) {
    /* altered for infinite scroll */
   echo '<div id="pagination">';
       echo '<div class="row align-center">';
           echo '<div class="column large-8">';
               echo '<div class="row">';
                   echo '<div class="column small-6">';
                       echo '<div id="prev" class="text-left">';
                           previous_posts_link( __( 'Newer', 'syrup' ) );
                       echo '</div>';
                   echo '</div>';
                   echo '<div class="column small-6">';
                       echo '<div id="next" class="text-right">';
                           next_posts_link( __( 'Older', 'syrup' ) );
                       echo '</div>';
                   echo '</div>';
               echo '</div>';
           echo '</div>';
       echo '</div>';
   echo '</div>';
    /* original */
    //  echo '<div class="row align-center" id="pagination">';
    //  echo '<div class="column small-6 large-4" id="prev">';
    //      previous_posts_link( __( '&larr; Newer Posts', 'syrup' ) );
    //  echo '</div>';
    //  echo '<div class="column small-6 large-5 text-right" id="next">';
    //      next_posts_link( __( 'Older Posts &rarr;', 'syrup' ) );
    //  echo '</div>';
    //  echo '</div>';
}
