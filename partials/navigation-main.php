<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
?>
<div id="header">
    <a href="<?php bloginfo('url'); ?>/" id="logo">
        <?php bloginfo('name'); ?>
    </a>
    <div class="menu-contain">
        <ul class="dropdown menu align-left" data-dropdown-menu>
            <?php
                $args = array(
                    'theme_location'  => 'primary_navigation',
                    'echo'            => true,
                    'container'      => '',
                    'items_wrap'      => '%3$s'
                );
                wp_nav_menu( $args );
            ?>
        </ul>
    </div>
</div>
