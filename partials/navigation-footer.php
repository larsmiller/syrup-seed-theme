<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
?>
<footer id="footer">
    <div class="row align-middle">
        <div class="columns small-12 medium-6 large-2">
            <a href="<?php bloginfo('url'); ?>/" id="footer-logo">
                <?php bloginfo('name'); ?>
            </a>
            <?php // echo social_menu('footer-social'); ?>
        </div>
    </div>
    <div class="row align-middle">
        <div class="columns small-12 medium-6 small-order-2 medium-order-1">
            <p id="copyright">&copy;&nbsp;<?php echo date('Y').'&nbsp;'.get_bloginfo('name'); ?></p>
        </div>
    </div>
</footer>
