<?php
function sample($data) {
    $section_id = $data['section_id'];
    ?>
    <div id="<?php echo $section_id; ?>" class="sample">
        <div class="row">
            <div class="column">
                <h2>Sample Component</h2>
                <p>Add stuff to it by editing the page.</p>
                <?php
                if (!empty($data['title'])) {
                    echo '<h1>'.$data['title'].'</h1>';
                }
                if (!empty($data['items'])) {
                    for ($x = 0; $x < count($data['items']); $x++) {
                        echo '<h3>'.$data['items'][$x]['title'].'</h3>';
                        echo '<p>'.$data['items'][$x]['text'].'</p>';
                    }
                }
                echo '<pre style="background:black;color:lime;">';
                print_r($data);
                echo '</pre>';
                ?>
            </div>
        </div>
    </div>
    <?php
}
