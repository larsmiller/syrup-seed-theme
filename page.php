<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
get_header();
if(function_exists('get_field')) {
    $components = get_field('components');
    if ($components) {
        require_once locate_template('/components/components.php');
        for ($i = 0; $i < count($components); $i++) {
            $function = $components[$i]['acf_fc_layout'];
            $data = $components[$i];
            $function($data);
        }
    }
}
if ($post->post_content != '') {
    ?>
    <div id="page" class="component-padding">
        <div class="row align-center">
            <div class="column large-8 medium-10">
                <?php the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </article>
            </div>
        </div>
    </div>
    <?php
}
get_footer();
?>
