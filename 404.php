<?php
/**
* @package WordPress
* @subpackage syrup
*/
get_header();
?>
<div id="page" class="component-padding">
    <div class="row align-center">
        <div class="column large-8 medium-10">
            <h1>We're sorry</h1>
            <p>Sorry, the page you requested may have been moved or deleted.</p>
            <a href="<?php bloginfo('url'); ?>" class="button">Homepage</a>
        </div>
    </div>
</div>
<?php get_footer(); ?>
