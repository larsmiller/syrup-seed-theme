<?php
/**
 * @package WordPress
 * @subpackage syrup
 */
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <?php
    if(function_exists('get_field')) {
        // loading scripts from ACF's Option page
        $scripts = get_field('scripts_head','option');
        for ($i = 0; $i < count($scripts); $i++) {
            echo $scripts;
        }
    }
    ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php wp_title(''); ?></title>
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/dist/img/favicon.png">
    <link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/dist/img/apple.png">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php
    if(function_exists('get_field')) {
        // loading scripts from ACF's Option page
        $scripts = get_field('scripts_body','option');
        for ($i = 0; $i < count($scripts); $i++) {
            echo $scripts;
        }
    }
    get_template_part( 'partials/navigation', 'main' );
