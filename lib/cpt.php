<?php
// Products
add_action( 'init', 'register_cpt_products' );

 function register_cpt_products() {

     $labels = array(
         'name' => _x( 'Products', 'products' ),
         'singular_name' => _x( 'Product', 'products' ),
         'add_new' => _x( 'Add New', 'products' ),
         'all_items' => _x( 'Products', 'products' ),
         'add_new_item' => _x( 'Add New Product', 'products' ),
         'edit_item' => _x( 'Edit Product', 'products' ),
         'new_item' => _x( 'New Product', 'products' ),
         'view_item' => _x( 'View Product', 'products' ),
         'search_items' => _x( 'Search Products', 'products' ),
         'not_found' => _x( 'No Products found', 'products' ),
         'not_found_in_trash' => _x( 'No Products found in Trash', 'products' ),
         'parent_item_colon' => _x( 'Parent Product:', 'products' ),
         'menu_name' => _x( 'Products', 'products' ),
     );

     $args = array(
         'labels' => $labels,
         'hierarchical' => false,
         'public' => true,
         'show_ui' => true,
//         'taxonomies' => array('category'),
         'supports' => array('title','editor','thumbnail'),
         'show_in_menu' => true,
         'rewrite' => array('slug' => 'products','with_front' => true),
     );

     register_post_type( 'products', $args );

    //  register_taxonomy( 'products-cat', array( 'products' ), array(
    //     'hierarchical' => true,
    //     'label' => __( 'Products Categories' ),
    //     'rewrite' => array( 'slug' => 'products-cat', 'with_front' => false )
    // ));

 }
