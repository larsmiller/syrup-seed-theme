<?php
function syrup_setup() {
	register_nav_menus(array(
		'primary_navigation' => __('Primary Navigation', 'syrup'),
		// 'footer_1' => __('Footer 1', 'syrup'),
		// 'footer_2' => __('Footer 2', 'syrup'),
		// 'footer_3' => __('Footer 3', 'syrup'),
		// 'footer_4' => __('Footer 4', 'syrup'),
	));
}
add_action('after_setup_theme', 'syrup_setup');

add_theme_support( 'post-thumbnails' );
// add_image_size( 'big_square', 800, 800, true);
// add_image_size( 'medium_square', 400, 400, true);
// add_image_size( 'x_large', 1400, 1000, false);