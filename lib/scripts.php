<?php
function syrup_scripts() {
	if ($_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '192.168.33.10') {
		wp_enqueue_style('style', get_template_directory_uri() . '/dist/css/app.css');
		wp_enqueue_script('app', get_template_directory_uri() . '/dist/js/app.js', '', '', false);
	} else {
		wp_enqueue_style('style', get_template_directory_uri() . '/dist/css/app.min.css');
		wp_enqueue_script('app', get_template_directory_uri() . '/dist/js/app.min.js', '', '', false);
	}
}
add_action('wp_enqueue_scripts', 'syrup_scripts');

// uncomment if using gform
// force gform javascript to load at the bottom of the page
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
	return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';
	return $content;
}
