<?php
function social_menu($id) {
    $social_fields = array(
        'twitter',
        'facebook'
    );
    $menu = '<ul id="'.$id.'" class="menu">';
    for ($i = 0; $i < count($social_fields); $i++) {
        $name = $social_fields[$i];
        $url = get_field($name,'option');
        if (!empty($url)) {
            $menu .= '<li><a href="'.$url.'" target="_blank"><span class="icon-'.$social_fields[$i].'"></span></a></li>';
        }
    }
    $menu .= '</ul>';
    return $menu;
}
?>
